#!/bin/bash
cidr_c=$1
ips_path=$2

[[ -z "${ips_path}" ]] && ips_path="${HOME}"

ips=$(sudo nmap -n -sn "172.1.${cidr_c}.0/24" -oG - | awk '/Up$/{print $2}')
echo ">> Found IPs: ${ips}"
echo "$ips" >> ./ips.txt
