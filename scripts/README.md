# bastion-drone-runners

1. Declare hostname in `~/.ssh/config`

   ```
   Host binah-dev-bastion
   HostName 52.18.145.10
   User ubuntu
   Port 22
   IdentityFile ~/.ssh/binah-ecs-tools-cluster
   ForwardAgent yes
   ```

1. Copy scripts folder to bastion

   ```bash
   # Assuming you defined bastion-dev-bastion in ~/.ssh/config
   $ scp -r scripts binah-dev-bastion:/home/ubuntu/
   ```

1. Add cron jobs

   ```bash
   $ crontab -e

   # Add the following lines

   # delete log file every 23:30, 28th of every month
   30 23 28 * * rm -f /tmp/cron.log >/dev/null

   # prune everything every 23:45, 28th of every month, drop logs in /tmp/cron.log
   45 23 28 * * bash $HOME/scripts/cron_prune_all.sh

   # prune volumes every day 00:00, drop logs in /tmp/cron.log
   0 2 * * * $HOME/scripts/cron_prune_volumes.sh

   ```
