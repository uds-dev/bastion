#!/bin/bash
# make sure ips.txt has private ip addresses of drone runners
# execute command to all drone runners
[[ -z "${SCRIPTS_PATH}" ]] && SCRIPTS_PATH="${HOME}/scripts"
cd "${SCRIPTS_PATH}"
cmd="$1"
filename="ips.txt"
exec 4<$filename
echo Start

while read -u4 ip; do
        echo $ip
        out=$(ssh "ec2-user@${ip}" "$cmd")
        echo "$out"
done