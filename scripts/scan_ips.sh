#!/bin/bash
SCRIPTS_PATH=$1
[[ -z "${SCRIPTS_PATH}" ]] && SCRIPTS_PATH="${HOME}/scripts"
echo ">> SCRIPTS_PATH = ${SCRIPTS_PATH}"

cd "${SCRIPTS_PATH}"
rm -f ips.txt
source nmap_scan.sh 1
source nmap_scan.sh 2
source nmap_scan.sh 3
echo ">> ips.txt="
cat ips.txt
