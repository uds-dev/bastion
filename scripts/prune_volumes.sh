#!/bin/bash
SCRIPTS_PATH=$1
[[ -z "${SCRIPTS_PATH}" ]] && SCRIPTS_PATH="${HOME}/scripts"
echo ">> SCRIPTS_PATH = ${SCRIPTS_PATH}"

cd "${SCRIPTS_PATH}"

source ssh_runners.sh "docker system prune --volumes -f"
