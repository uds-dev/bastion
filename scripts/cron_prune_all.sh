#!/bin/bash
SCRIPTS_PATH=$1
[[ -z "${SCRIPTS_PATH}" ]] && SCRIPTS_PATH="${HOME}/scripts"
echo ">> SCRIPTS_PATH = ${SCRIPTS_PATH}" >> /tmp/cron.log
echo ">> Timestamp = $(date +%d-%m-%Y_%H-%M-%S)" >> /tmp/cron.log
cd "${SCRIPTS_PATH}"
source scan_ips.sh >> /tmp/cron.log
source ssh_runners.sh "docker system prune --volumes --all -f" >> /tmp/cron.log
