locals {
  common_tgs = {
    Environment = var.environment
    Name        = var.name
  }
}

variable "name" {
  type = string
}

variable "environment" {
  type = string
}

variable "associate_eip" {
  type    = bool
  default = true
}

variable "vpc_id" {
  type = string
}

variable "key_name" {
  type = string
}

variable "public_subnet_id" {
  type = string
}

variable "image_id" {
  type = string
}

variable "allowed_cidrs" {
  type = list(string)
}

variable "delete_on_termination" {
  type    = bool
  default = true
}

variable "volume_size" {
  type    = number
  default = 20
}
