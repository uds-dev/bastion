resource "aws_instance" "bastion" {
  ami = var.image_id
  root_block_device {
    volume_type           = "gp2"
    volume_size           = var.volume_size
    delete_on_termination = var.delete_on_termination
  }
  key_name                    = var.key_name
  subnet_id                   = var.public_subnet_id
  instance_type               = "t3.micro"
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  associate_public_ip_address = true
  tags = {
    Name = "${var.name}-bastion"
  }
}


resource "aws_security_group" "bastion" {
  name   = "bastion-security-group"
  vpc_id = var.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = var.allowed_cidrs
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.name}-bastion-sg"
  }
}

resource "aws_eip" "bastion" {
  count    = var.associate_eip ? 1 : 0
  vpc      = true
  instance = aws_instance.bastion.id
  tags = {
    Name = "${var.name}-eip"
  }
}
