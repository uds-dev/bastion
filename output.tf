output "bastion_public_ip" {
  value = aws_instance.bastion.public_ip
}

locals {
  bastion_eip = var.associate_eip ? aws_eip.bastion[0].public_ip : aws_instance.bastion.public_ip
}

output "bastion_eip" {
  value = local.bastion_eip
}
